export const pointsDist = (pointA: point, pointB: point) => {
  return Math.sqrt(Math.pow(pointA.x - pointB.x, 2) + Math.pow(pointA.y - pointB.y, 2))
}

export const createPoint = (maxWidth: number, maxHeight: number) => {
  return {
    x: Math.random() * maxWidth,
    y: Math.random() * maxHeight
  }
}

export const createPointInCircle = (center: { x: number, y: number }, radius: number) => {
  let r = radius * Math.sqrt(Math.random())
  let theta = Math.random() * 2 * Math.PI
  return {
    x: center.x + r * Math.cos(theta),
    y: center.y + r * Math.sin(theta)
  }
}